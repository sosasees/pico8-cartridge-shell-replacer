# shell replacer for PICO-8 cartridges

most of the important information about this program
is in [this Lexaloffle BBS post](https://www.lexaloffle.com/bbs/?tid=50563)

this GUI tool was made in
[Godot Engine](https://godotengine.org/) version [4.0 Beta 10](https://godotengine.org/article/dev-snapshot-godot-4-0-beta-10)

## contents

- LICENSES
   - scripts
   - images

## LICENSES

### scripts

i licensed most scripts — which are too easy to recreate by
coincidence — under
[MIT License](https://codeberg.org/Sosasees/mit-license/raw/branch/2022/LICENSE):
   - [res://cart_preview.tscn](cart_preview.tscn):Script
   - [res://main.tscn](main.tscn)/
	  - MainArea/SaveButton:Script
	  - FileDialogs/
		 - Save:Script
		 - Load/
			- Cart:Script
			- Shell:Script
	  - AboutDialog:Script
	  - AboutDialog/VBoxContainer/Control/VBoxContainer/CloseButton:Script

i licensed the very distinct script that is almost impossible to
recreate by coincidence under
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0-standalone.html)
to boost the free software market:
   - [res://main.tscn](main.tscn)/Images:Script

### images

#### cart placeholder

![](cart_placeholder.webp)

[res://cart_placeholder.webp](cart_placeholder.webp) is a changed
version of the ['more carts' button](https://archive.vn/9yXRD) on the
[pico-8 homepage](https://archive.vn/XAWsf). i did not ask
[zep](https://www.lexaloffle.com/bbs/?uid=1) for permission before
using it.
because it's based off the standard pico-8 cartridge template — which
PICO-8 uses to save .p8.png cartridiges that can have any license — i
can mark this image with
[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
— which i did


#### app icon

![](icon.webp)

[res://icon.webp](icon.webp) is an original image that i made for this
project. i marked it with
[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
